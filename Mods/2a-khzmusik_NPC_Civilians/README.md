# NPC Civilians

Adds "Civilian" human NPCs to the game.

All characters were created by khzmusik, using assets that are either free for re-use or CC-BY.
See the Technical Details section for more information and asset credits.

## Civilians

The pack contains these characters:

### Test Female
![Test Female](./UIAtlases/UIAtlas/npcTestFemaleKhz.png)

### Test Male
![Test Male](./UIAtlases/UIAtlas/npcTestMaleKhz.png)

### Female Aged
![Female Aged](./UIAtlases/UIAtlas/npcFemaleAgedKhz.png)

### Female Scrubs
![Female Scrubs](./UIAtlases/UIAtlas/npcFemaleScrubsKhz.png)

### Female Young 1
![Female Young 1](./UIAtlases/UIAtlas/npcFemaleYoung1Khz.png)

### Female Young 2
![Female Young 2](./UIAtlases/UIAtlas/npcFemaleYoung2Khz.png)

### Female Young 3
![Female Young 3](./UIAtlases/UIAtlas/npcFemaleYoung3Khz.png)

### Female Young 4
![Female Young 4](./UIAtlases/UIAtlas/npcFemaleYoung4Khz.png)

### Female Young 5
![Female Young 5](./UIAtlases/UIAtlas/npcFemaleYoung5Khz.png)

### Male Aged
![Male Aged](./UIAtlases/UIAtlas/npcMaleAgedKhz.png)

### Male Aged Scrubs
![Male Aged Scrubs](./UIAtlases/UIAtlas/npcMaleAgedScrubsKhz.png)

### Male Aged 2
![Male Aged 2](./UIAtlases/UIAtlas/npcMaleAged2Khz.png)

### Male Cowboy
![Male Cowboy](./UIAtlases/UIAtlas/npcMaleCowboyKhz.png)

### Male Young 1
![Male Young 1](./UIAtlases/UIAtlas/npcMaleYoung1Khz.png)

### Male Young 2
![Male Young 2](./UIAtlases/UIAtlas/npcMaleYoung2Khz.png)

### Male Young 3
![Male Young 3](./UIAtlases/UIAtlas/npcMaleYoung3Khz.png)

All of them are in the Whiteriver faction, and can be hired.

Every character can wield all supported NPC weapons.

## Dependent and Compatible Modlets

This modlet is dependent upon the `0-NPCCore` modlet,
and that modlet is in turn dependent upon the `0-SCore` modlet.

So far as I am aware, it should work with any versions of those modlets.

## Technical Details

This modlet includes new non-XML resources (Unity assets).
These resources are _not_ pushed from server to client.
For this reason, this modlet should be installed on both servers and clients.

### Spawning

These NPCs are spawned into entity groups used by vanilla biome spawners.
NPCs are spawned with probabilities that should not overwhelm zombies.

* `ZombiesAll`:
    Characters that are more likely to be scouters/scavengers are spawned at normal probabilities.
    Characters that are more likely to stay at home/work are spawned at lower probabilities.
* `ZombiesNight`:
    Characters that are more likely to be scouters/scavengers are spawned at lower probabilities.
    Characters that are more likely to stay at home/work are not spawned.
* `SnowZombies`:
    Characters that are more likely to be scouters/scavengers are spawned at normal probabilities.
    Characters that are more likely to stay at home/work are not spawned.

Since these entity groups are not gamestaged,
their spawn probabilities are adjusted according to weapon damage.
For example, if an NPC with a club would spawn with a probability of 1,
an NPC with a rocket launcher would spawn with a probability of 0.1.

They are also spawned into the entity groups used by NPC sleeper volumes:

* `npcWhiteriver*`
* `npcFriendly*`

For those entity groups, the NPCs match the weapons and probabilities of the existing NPC Core
characters in those groups.

### How the characters were created

The character models were created using one of two programs:

* Mixamo Fuse - a free program for making human-like characters.
    Unfortunately, after Adobe bought it, it was discontinued and is no longer supported.
    But it can still be used, and is still very good.
* [MakeHuman](http://www.makehumancommunity.org) -
    an open source program for making human characters.
    It includes many community assets.

Once the models were created, I exported them to `.obj` files, and rigged them in
[Mixamo](https://www.mixamo.com).
From Mixamo, I exported them to Unity `.fbx` files.

I then imported them into Unity to set up ragdolls, rigging, tags, material shaders, etc.

None of this would be possible were it not for the help of Xyth and Darkstardragon - thank you!

If you would like to know how to do all this yourself,
Xyth has an excellent series of tutorials on YouTube:

https://www.youtube.com/c/DavidTaylorCIO/playlists

### Re-use in other mods/modlets

Any rights that I hold in these characters, I hereby place into the public domain (CC0).
Feel free to re-use them in any of your mods or modlets.

However, I don't hold the rights to all the assets in the models.
I have used assets from Mixamo Fuse and MakeHuman, and do not hold any rights in those assets.
Those rights are retained by Fuse, and/or the members of the MakeHuman community.

This sounds worse than it is.
Both the Fuse and MakeHuman assets are released under very lenient licenses,
and it should not be any problem for mod authors to include them in their mods:

* Characters created in Mixamo Fuse may be used in any game (commerical or not) for free,
    though you cannot repackage and sell the assets.
* MakeHuman characters can include assets from the MakeHuman team, or community assets.
    The MakeHuman assets are CC0, and the community assets are usually CC0 or CC-BY.
    Distribution of characters that include CC-BY assets **must** abide by the terms of that license,
    and include appropriate credits for those assets.

Obviously, the characters also use assets from The Fun Pimps (sounds, animations, etc).
The Fun Pimps retain the rights in those assets.

### Asset Credits

These assets were used in the creation of the characters in this modlet.
The assets were created for, and used in, MakeHuman.

* **Double MH Braid 01**  
    author:  Elvaerwyn  
    license: [CC0](https://creativecommons.org/share-your-work/public-domain/cc0) 
* **Eve Hill's Young_Hispanic_Female**  
    author: Eve Hill Sisters Production Company / 3d.sk / jimlsmith9999  
    license: license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **female half-boots**  
    author:  punkduck  
    license: license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **French Braid 01 Variation**  
    author:  Elvaerwyn  
    license: [CC0](https://creativecommons.org/share-your-work/public-domain/cc0) 
* **lace-choker**  
    author:  punkduck  
    license: [CC0](https://creativecommons.org/share-your-work/public-domain/cc0) 
* **Necklace (Native American Fashion)**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **Retro Top**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **skin male african middleage**  
    author:  Mindfront  
    license: [CC0](https://creativecommons.org/share-your-work/public-domain/cc0) 
* **Skirt (Native American Fashion)**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **String 4**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **Tight Jeans (female)**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 

If I have missed any assets used in these characters, please let me know,
and I will credit the author or remove the character, as appropriate.
