## DO NOT PUSH TO THIS REPO
This repo is for Sphereii Modlauncher. Any edits, commits, etc., should be made to the repo parent directory, not "Mods."

## AECmod_v245e
This is an open beta test of the single player and PVE version of the overhaul.
Your feedback is encouraged and necessary for the final version!

# AECmod v245e Overhaul PVE Server [12 slots ]
45.35.207.75:26960

# Package Description
AECmod v245e is a total overhaul mod for 7 Days to Die that introduces story elements, NPC characters with dialog based objectives and quests, new POIs, orchestral music scores from action horror movie classics, and a beautiful, hand-crafted 12k map recreation of Warm Springs, Lake Moomaw, the Jackson River, Covington, Virginia.


Showcasing some of the most powerful mods available, AECmod uses SCore and XNPCCore to present new functionality, visceral situations, and daunting challenges. Players can lockpick doors, trigger dynamic music, use blocks to spawn NPCs, then use those NPC to guard, patrol, or bringing more firepower to the fight. Although there is a dark secret and rich story hidden beneath the mountains of Warm Springs, there is wild fun in a nomadic playthrough, where players makes no allegiances, no promises, and no guarantees to anyone other than themselves. Will you be a hero? Will you be a villain? Is there even a such thing in a post-World War 3 society? 


AECmod is a great place for people who are looking for a familiar 7 Days to Die experience, enhanced by new themes, gameplay, and strategy elements.


The current build is a demo version of AECmod, which puts the player directly on top of D-LAMP, the star attraction POI of the mod. Here the player will meet KACEY GELLAR, a member of a small party of survivors who recently discovered the facility. There is also a rough version of CAMP BAUDRILLARD located in the Southeast portion of the map, as well as Sandia Peaks, New Mexico, located in the Southwest.

This mod is in it's very early stages, but it has been play-tested for countless hours on dedicated servers, Steam multiplayer, and single player (all on a potato i3 Radeon RX550 computer).

# FEATURES
- Powered by SCore and XNPCCore!
- Vehicle Madness Overhaul Mod included in the package!
- Faction based NPC companions and enemies - soldiers, raiders, and civilians!
- NPC packs by KhzMusik and Xyth
- Lockpicking modlet by Sphereii and Xyth
- Custom water animations by Cpt Krunch
- Handcrafted 12k map by Goldmanvision and Cpt. Krunch, modelled after the real world mountain region of Warm Springs, Virginia, Lake Moomaw, and the city of Covington in North America.
- Southwestern desert region - "Sandia Peaks, New Mexico"
- New custom POIs, including a 1.5km underground roadway, Camp Baudrillard, a massive underground Atomic Energy Commission facility called D-LAMP, and more.
- Expanded NPC names - over 750 variations of names!
- Highly curated experience: hand-placed POIs, custom zoning maps, variable rebalancing, and more!

# HOW TO INSTALL
- Locate your Worlds folder and game install folder for 7 Days to Die (usually SteamLibrary>steamapps>common>7 Days To Die).

- Extract the "AEC_WarmSprings_Virginia_v245e_rc1" inside GeneratedWorlds to install the AECmod world map.

- Extract the "Mods" folder to your 7 Days to Die directory to install the AECmod Overhaul Package.

# THIRD PARTY MODS
- SCore
- XNPCCore
- Vehicle Madness
- Khzmusik Civilians
- Xyth's Soldier NPC Pack
- Xyth's Raiderz NPC Pack
- Cpt Krunch's Water Animation Blocks


Special thanks to Krunch for his work on the Warm Springs map
For newer versions of 7 Days to Die (a20.4 and above), the Mods folder is now located in your %appdata% > 7DaysToDie folder (see 7 Days to Die changelog for details).


# AECmod on Guppy's Unofficial 7DtD Modding Server
https://discord.com/channels/243577046616375297/965771485379706910



# AECmod v245e Public PVE Server - SCore / XNPCcore / Vehicle Madness
45.35.207.75:26960
